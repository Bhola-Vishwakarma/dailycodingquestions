#Hi, here's your problem today. This problem was recently asked by Amazon:
#
#Given an array of n positive integers and a positive integer s, find the minimal length of a contiguous subarray of which the sum ≥ s. If there isn't one, return 0 instead.
#
#Example:
#
#Input: s = 7, nums = [2,3,1,2,4,3]
#Output: 2
#
#Explanation: the subarray [4,3] has the minimal length under the problem constraint.

class Solution:

    def printArr(self,arr,nums):
        sum =0
        print("Elements of an array in are ")
        for x in range(arr[0],arr[1]):
            sum=sum+nums[x]
            print("["+str(x)+"] = "+str(nums[x]))
        
        print("Sum is:"+str(sum))
    
    def minSubArraySum(self,nums,sum):
        p=0
        q=1
        currSum=0
        
        if len(nums)==0:
            return None
        
        while True:
            if q==len(nums):
                break
            
            subVal = abs(nums[q]-nums[q-1])
            
            if subVal > 1:
                p=q
                q+=1
                currSum=0
            else:
                if currSum==0:
                    currSum = currSum + nums[q] + nums[q-1]
                else:
                    currSum = currSum + nums[q] 
                    
                if currSum==sum:
                    break
                
                q+=1
                    
         
        if currSum==sum:
            arr=[p,q]
        else:
            arr=None
        
        return arr
nums =  [2,3,1,2,3,3,3];              
arr = Solution().minSubArraySum(nums,12)
Solution().printArr(arr,nums)



    

