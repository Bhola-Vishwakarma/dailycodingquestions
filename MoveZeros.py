# Hi, here's your problem today. This problem was recently asked by Facebook:

# Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.

# Example:

# Input: [0,1,0,3,12]
# Output: [1,3,12,0,0]

# You must do this in-place without making a copy of the array.
# Minimize the total number of operations.

# Here is a starting point:

def moveZero(nums):
    i=0;
    j=1;

    while(True):
        
        if len(nums) == j:
            break;   
        
        if nums[i]==0 and nums[j]>0:
            swap(nums,i,j)
            i+=1
            j+=1            
            
        elif nums[i]==0 and nums[j]==0:
             j+=1    

def swap(nums,i,j):
    tmp     = nums[j]
    nums[j] = nums[i]
    nums[i] = tmp


nums = [0, 0, 0, 2, 0, 1, 3, 4, 0, 0]
moveZero(nums)
print(nums)

